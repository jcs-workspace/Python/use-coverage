# use-coverage

Following tutorial [100% CODE COVERAGE - Think You're Done? Think AGAIN.☝](https://www.youtube.com/watch?v=jmP3fp_BhmE&t=941s).

To generate report:

```sh
$ coverage html
```
